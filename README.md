# click-test

This is sample code for python click test case.

## Getting started

```
$ export PYTHONPATH=.
$ pip install -r requirements.txt
$ python commands/cmd.py 
Usage: cmd.py [OPTIONS] COMMAND [ARGS]...

Options:
  --opt1 TEXT
  --version
  --help       Show this message and exit.

Commands:
  hello
$ python commands/cmd.py --version
Version 1.0
$ python commands/cmd.py hello
Hello World!
$
```
## Test

```
pytest -v --cov-branch --cov=commands --cov-report=html
```
