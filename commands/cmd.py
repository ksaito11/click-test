import click

from commands.hello import hello

def print_version(ctx, param, value):
    click.echo('Version 1.0')
    ctx.exit()

@click.group()
@click.option('--opt1')
@click.option('--version', is_flag=True, callback=print_version,
              expose_value=False, is_eager=True)
@click.pass_context
def cmd(ctx, **kwargs):
    ctx.obj = kwargs

def main():
    cmd.add_command(hello)
    cmd(auto_envvar_prefix='HELLOCLI')

if __name__ == '__main__':
    main()
