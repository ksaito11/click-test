from click.testing import CliRunner
import click
import pytest

from commands.cmd import cmd, main
from commands.hello import hello

def test_version():
    runner = CliRunner()
    result = runner.invoke(cmd, ["--version"])
    assert result.exit_code == 0

def test_help():
    runner = CliRunner()
    result = runner.invoke(cmd)
    assert result.exit_code == 0

def test_hello():
    runner = CliRunner()
    result = runner.invoke(hello)
    assert result.exit_code == 0