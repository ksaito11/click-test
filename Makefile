all: cov

cov:
	pytest -v --cov-branch --cov=commands

htmlcov:
	pytest -v --cov-branch --cov=commands --cov-report=html
